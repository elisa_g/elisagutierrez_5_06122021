
// Requête API : récupération des produits

fetch('http://localhost:3000/api/products')
	.then(function(response) {
		if (response.ok) {
			return response.json();
		} else {
			alert('Bug');
		}
	})

	.catch(function(error) {
		console.log(error)
	});
	
// Insertion des produits sur la page Accueil

async function getProducts() {
	let url = ('http://localhost:3000/api/products');
	try {
		let response = await fetch('http://localhost:3000/api/products')
		return await response.json();
	}
	catch (error) {
		console.log(error)
	}
}


async function renderProducts() {
	let products = await getProducts();
	let html = '';
	products.forEach(product => {
		let htmlSection = `<a href=./product.html?id=${product._id}>
							<article class='product'>
								<img src='${product.imageUrl}' alt='${product.altTxt}'>
								<h3 class='productName'>${product.name}</h3>
								<p class=productDescription>${product.description}</p>
							</article>
						</a>`;

		html += htmlSection;
	});

	let items = document.querySelector('.items');
	items.innerHTML = html;
}

renderProducts();
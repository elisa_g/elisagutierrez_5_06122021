let items = JSON.parse(localStorage.getItem('product'));

// Affichage du panier
function getCart() {
    // Si le panier est vide
    if (items === null || items == 0) {
        const statusCart = document.querySelector('#cart__items');
        const emptyCart = `<p>LE PANIER EST VIDE</p>`;
        statusCart.innerHTML = emptyCart;
        document.querySelector('.cart__order__form').hidden = true;
        document.querySelector('#totalQuantity').innerHTML = 0;
        document.querySelector('#totalPrice').innerHTML = 0;
        return false;
    // Si le panier n'est pas vide
    } else {
        return true;
    }
};

// Affichage des articles dans le panier
async function displayCart() {
    let totalPrice = 0;
    let totalQuantity = 0;
    let cartContent = '';
    for (let product of items) {
        await fetch(`http://localhost:3000/api/products/${product.id}`)
            .then(result => result.json())
            .then(article => {
                totalPrice += parseInt(article.price) * parseInt(product.quantity);
                totalQuantity += parseInt(product.quantity);
                cartContent += 
                `<article class='cart__item' data-id='${product.id}' data-color='${product.color}'>
                <div class='cart__item__img'>
                  <img src='${article.imageUrl}' alt='${article.altTxt}'>
                </div>
                <div class='cart__item__content'>
                <div class='cart__item__content__description'>
                  <h2>${article.name}</h2>
                  <p>${product.color}</p>
                  <p>${article.price * product.quantity} €</p>
                </div>
                <div class='cart__item__content__settings'>
                    <div class='cart__item__content__settings__quantity'>
                      <p>Qté : </p>
                      <input type='number' class='itemQuantity' name='itemQuantity' min='1' max='100' value='${product.quantity}'>
                    </div>
                    <div class='cart__item__content__settings__delete'>
                      <p class='deleteItem'>Supprimer</p>
                    </div>
                  </div>
                </div>
              </article>`;
            })
            .catch(error => {
                console.log(error);
            });
    }
    document.querySelector('#cart__items').innerHTML = cartContent;
    document.querySelector('#totalQuantity').innerHTML = totalQuantity;
    document.querySelector('#totalPrice').innerHTML = totalPrice;

deleteProduct();
changeQuantity();
  
};

// Supprimer des articles du panier
function deleteProduct() {
    // Parcourir le localstorage
    let deleteBtn = document.querySelectorAll('.deleteItem');
      for(let article = 0; article < items.length; article++) {
        deleteBtn[article].addEventListener('click', (event) => {
          event.preventDefault();
          // Affecter à une variable 'valueId' la valeur  de 'id' et de 'color'
          // d'un élément d'un tableau contenu dans le JSON
          let valueId = deleteBtn[article].closest('article').dataset.id;
          let valueColor = deleteBtn[article].closest('article').dataset.color;
          if ((valueId && valueColor)) {
              // Si items non vide
              if (items.length > 1) {   
                // Remove l'entrée du tableau dont l'élément id correspond à 'value'
                items.splice(article,1);
                localStorage.setItem('product', JSON.stringify(items));  
              } else {
                  localStorage.removeItem('product');
                }
              location.reload();
              window.location.hash = 'cart__items';
              alert('Article supprimé du panier');
            }
            // Si items vide 
            if (items === null || items === 0) {
              localStorage.removeItem('product');
            }
        })
      }
    };

// Modifier la quantité d'articles dans le panier
function changeQuantity() {
  const min = 1;
  const max = 100;
  let quantityBtn = document.querySelectorAll('.itemQuantity');
  for(let article = 0; article < items.length; article++) {
    quantityBtn[article].addEventListener('change', (event) => {
      event.preventDefault();
      // Affecter à une variable 'valueId' la valeur  de 'id' et de 'color'
      // d'un élément d'un tableau contenu dans le JSON
      let valueId = quantityBtn[article].closest('article').dataset.id;
      let valueColor = quantityBtn[article].closest('article').dataset.color;
      if ((valueId && valueColor)) {
        let valueNewQuantity = event.target.value;
        if (valueNewQuantity < min) {
          alert('Veuillez choisir au moins' + min + 'article');
        } else if (valueNewQuantity > max) {
          alert('Merci de ne pas dépasser' + max + 'articles');
        } else {
          // Est-ce que la page correspond toujours au localstorage ?
          // Ajout en plus depuis le chargement par exemple.
          if ((items[article].id === valueId) && (items[article].color === valueColor)) {
            items[article].quantity = parseInt(valueNewQuantity);
            localStorage.setItem('product', JSON.stringify(items));
            location.reload();
            window.location.hash = 'cart__items';
          } else {
            location.reload();
            window.location.hash = 'cart__items';
            alert('Panier non à jour. Rechargement.');
          }
        }
      }
     })
  }
};

// Vérification du formulaire
function formCheck() {

  // Validation prénom
  const firstNameCheck = () => {
      const firstName = document.querySelector('#firstName');
      const firstNameErrorMsg = document.querySelector('#firstNameErrorMsg');
      if (/^[A-Za-z-éèêëï]+[a-zéèêëï]$/.test(firstName.value)) {
          firstNameErrorMsg.innerHTML = '';
          return true;
      }
      else {
          firstNameErrorMsg.innerHTML = 'Veuillez renseigner un prénom valide.';
      }
  };

  // Validation nom
  const lastNameCheck = () => {
      const lastName = document.querySelector('#lastName');
      const lastNameErrorMsg = document.querySelector('#lastNameErrorMsg');
      if (/^[A-Za-z- 'éèêëï]+[a-z- 'éèêëï]$/.test(lastName.value)) {
          lastNameErrorMsg.innerHTML = '';
          return true;
      }
      else {
          lastNameErrorMsg.innerHTML = 'Veuillez renseigner un nom valide.';
      }
  };

  // Validation adresse
  const addressCheck = () => {
      const address = document.querySelector('#address');
      const addressErrorMsg = document.querySelector('#addressErrorMsg');
      if (/^[0-9A-Za-z- '.éèêëïà]+[0-9A-Za-z- '.éèêëïà]$/.test(address.value)) {
          addressErrorMsg.innerHTML = '';
          return true;
      }
      else {
          addressErrorMsg.innerHTML = 'Veuillez renseigner une adresse valide.';
      }
  };

  // Validation ville
  const cityCheck = () => {
      const city = document.querySelector('#city');
      const cityErrorMsg = document.querySelector('#cityErrorMsg');
      if (/^[A-Za-z- 'éèêëïà]+[A-Za-zéèêëïà]$/.test(city.value)) {
          cityErrorMsg.innerHTML = '';
          return true;
      }
      else {
          cityErrorMsg.innerHTML = 'Veuillez renseigner une ville valide.';
      }
  };

  // Validation email
  const emailCheck = () => {
      const email = document.querySelector('#email');
      const emailErrorMsg = document.querySelector('#emailErrorMsg');
      if (/^[\w\.]+@([\w-]+\.)+[\w-]{2,4}$/.test(email.value)) {
          emailErrorMsg.innerHTML = '';
          return true;
      }
      else {
        emailErrorMsg.innerHTML = 'Veuillez renseigner un email valide.';
      }
  };

  // Vérification que tout les inputs sont valides
  if (firstNameCheck() &&
      lastNameCheck() &&
      addressCheck() &&
      cityCheck() &&
      emailCheck()) {
      return true;
  }
  else {
      return false;
  }
}

// Envoi de la commande
function orderSend() {
    const orderBtn = document.querySelector('#order');
    orderBtn.addEventListener('click', (event) => {
        event.preventDefault();
        if (formCheck()) {
            let contact = {
                firstName: document.querySelector('#firstName').value,
                lastName: document.querySelector('#lastName').value,
                address: document.querySelector('#address').value,
                city: document.querySelector('#city').value,
                email: document.querySelector('#email').value,
            };
            let products = [];
            items.forEach(product => {
                products.push(product.id)
            });
            let user = {
                contact,
                products
            };
            fetch('http://localhost:3000/api/products/order', {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(user),
            })
                .then(result => {
                  return result.json();
                })
                .then(result => {
                    window.location.href = `./confirmation.html?id=${result.orderId}`;
                    localStorage.clear();
                })
                .catch(error => {
                    console.log('error');
                });
        }
        else {
            alert('Vérifiez que vos informations sont valides');
        }
    });
};

// Confirmation de la commande
function confirmOrder() {
  const orderConfirmId = url.searchParams.get('id');
  document.querySelector('#orderId').innerHTML = orderConfirmId;
}


// Récupération des données du localstorage
let url = new URL(window.location.href);
if (url.pathname.match(/\/cart\.html/)) {
  getCart();
  displayCart();
  orderSend();
} else if (url.pathname.match(/\/confirmation\.html/)) {
  confirmOrder();
}

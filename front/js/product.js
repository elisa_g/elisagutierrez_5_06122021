// Récupération de l'id

var str = window.location.href;
var url = new URL(str);
var productId = url.searchParams.get('id');

        
// Requête API : récupération des produits

    async function getProduct() {

        let url2 = ('http://localhost:3000/api/products/');
        try {
            if (productId !== null) {
                let response = await fetch(url2 + productId)
                return await response.json();
            } else {
                alert('Bug');
            }
        }
        catch (error) {
            console.log(error)
        }
    }

// Insertion des paramètres dans la page Produit
    
    async function renderProduct() {
        let product = await getProduct();

        let articleImg = `<img src='${product.imageUrl}' alt='${product.altTxt}'>`;
        let itemImg = document.querySelector('.item__img');
        itemImg.innerHTML = articleImg;

        let articleTitle = `<h1>${product.name}</h1>`;
        let itemTitle = document.querySelector('#title');
        itemTitle.innerHTML = articleTitle;

        let articlePrice = `<span>${product.price}</span>`;
        let itemPrice = document.querySelector('#price');
        itemPrice.innerHTML = articlePrice;

        let articleDescription = `<p id=description>${product.description}</p>`;
        let itemDescription = document.querySelector('#description');
        itemDescription.innerHTML = articleDescription;

        let itemColors = document.querySelector('#colors')
        for (i = 0; i < product.colors.length; i++) {   
            let option = document.createElement('option');
            option.innerHTML = product.colors[i];
            itemColors.appendChild(option);
        }
    addToCart();
    }

renderProduct();

// Ajouter au panier
function addToCart() {
	let btnCart = document.querySelector('#addToCart');
	btnCart.addEventListener('click', (event) => {
		event.preventDefault();

		let productColor = document.querySelector('#colors').value;
		let productQuantity = document.querySelector('#quantity').value;

		if (productColor == '') {
			alert('Sélectionnez une couleur');
			return;
		} else if (productQuantity == 0) {
			alert('Indiquez une quantité');
			return;
		}
        let infoProduct = {
            id : productId,
            color : productColor,
            quantity : parseInt(productQuantity)
        };
		const popupConfirm = () => {
			if (window.confirm(`Votre article a été ajoutée au panier`)) {
				window.location.href = 'cart.html';
			}
		}
		// Si localstorage existe
		if (JSON.parse(localStorage.getItem('product')) !== null) {
			// Récupération du localstorage
			let items = JSON.parse(localStorage.getItem('product'));
			// Parcours du localstorage
			for (let article = 0; article < items.length; article++) {
				// Récupération des valeurs
				let valueId = items[article].id;
				let valueColor = items[article].color;
				let valueQuantity = items[article].quantity;
				// Si JSON valide
				if (valueId && valueColor && valueQuantity) {
					// Si même article
					if ((valueId === infoProduct.id) && (valueColor === infoProduct.color)) {
						// Modification de notre tableau temporaire
						items[article].quantity = parseInt(valueQuantity) + parseInt(infoProduct.quantity);
						// Réécriture du localstorage avec notre tableau
						localStorage.setItem('product', JSON.stringify(items));
						// Confirmation
						popupConfirm();
                        // On sort de la boucle
                        break;
					// Si nouvel article
					} else {
						// Ecriture du localstorage avec notre tableau
                        // Jusqu'a la fin de notre localstorage (panier) actuel
                        if (article === items.length - 1) {
                            items.push(infoProduct);
                            localStorage.setItem('product', JSON.stringify(items));
                            // Confirmation
                            popupConfirm();
                            // On sort de la boucle
                            break;
                        }
					}
				} else {
                    console.log('Bad localstorage!')
                }
			}
        // Si il n'y a pas de localstorage existant
		} else {
			// Création du localstorage
            let items = [];
            items.push(infoProduct);
			// Ecriture du localstorage avec notre tableau
			localStorage.setItem('product', JSON.stringify(items));
			// Confirmation
			popupConfirm();
		}
	})
};